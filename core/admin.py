from django.contrib import admin
from .models import Usuario

# Register your models here.

class UsuarioAdmin(admin.ModelAdmin):
    list_display = ['Nombre','Sexo','Correo_Electronico', 'Contraseña','Rut']
    search_fields = ['Rut']
    list_filter = ['Nombre']
    list_per_page = 10

admin.site.register(Usuario,UsuarioAdmin)