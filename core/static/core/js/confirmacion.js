function confirmarEliminacion(id){
    Swal.fire({
        title: 'Estas seguro?',
        text: "No podras deshacer",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminar',
        cancelButtonText: 'Cancelar',
      }).then((result) => {
        if (result.isConfirmed) {
         //redirigir
         window.location.href="/eliminar-usuario"+id+"/";

    
        }
      })
}