
from django import forms
from django.forms import ModelForm
from .models import Usuario
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class UsuarioForm(ModelForm):

    Nombre = forms.CharField(min_length=2,max_length=100)
    Sexo = forms.CharField(min_length=5,max_length=6)
    Correo_Electronico = forms.CharField(min_length=12,max_length=100)
    Contraseña = forms.CharField(min_length=5,max_length=30)
    Rut= forms.IntegerField(min_value=9, max_value=9)
    
    class Meta:
        model = Usuario
        fields = ['Nombre','Sexo','Correo_Electronico','Contraseña','Rut']

    
class CustomUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['first_name','last_name','email','username','password1','password2']