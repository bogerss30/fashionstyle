from django.shortcuts import render,redirect
from .models import Usuario
from .forms import UsuarioForm,CustomUserForm
from django.contrib.auth.decorators import login_required,permission_required
from django.contrib.auth import login, authenticate

# Create your views here.

def home(request):

    return render(request,'core/home.html')

def galeria(request):

    return render(request,'core/galeria.html')

def listado_usuarios(request):
    usuario = Usuario.objects.all()
    data = {
        'usuario':Usuario
    }  
    return render(request, 'core/listado_usuarios.html', data)

def nuevo_usuarios(request):
    data = {
        'form':UsuarioForm()
    }

    if request.method == 'POST':
        formulario = UsuarioForm(request.POST)
        if formulario.is_valid():
            formulario.save()
            data['mensaje'] = "Registro Exitoso"
        data['form'] = formulario
            
    return render(request, 'core/nuevo_usuarios.html',data )

def modificar_usuarios(request,id):
    usuario = Usuario.objects.get(id=id)
    data ={
        'form':UsuarioForm(instance=usuario)
    }

    if request.method == 'POST':
        formulario = UsuarioForm(data=request.POST, instance=Usuario)
        if formulario.is_valid():
            formulario.save()
            data['mensaje'] = "Modificacion exitosa"
            data['form'] = formulario

    return render(request,'core/modificar_usuarios.html', data)

def eliminar_usuario(request,id):
    Usuario = Usuario.objects.get(id = id)
    Usuario.delete()

    return redirect(to="listado_usuarios")
    
def contacto(request):

    return render(request,'core/contacto.html')

def registrar(request):
    data={
        'form':CustomUserForm()

    }
    if request.method =='POST':
        formulario = CustomUserForm(request.POST)
        if formulario.is_valid():
            formulario.save()
            #autenticar al usuario e ir al inicio
            username = formulario.cleaned_data['username']
            password = formulario.cleaned_data['password1']
            user = authenticate(username = username, password=password)
            login(request,user)
            return redirect(to='home')
    return render(request, 'registration/registrar.html',data)

























def loginvista(request):

    return render(request,'core/loginvista.html')

def registrarvista(request):

    return render(request,'core/registrarvista.html')




