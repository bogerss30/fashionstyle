from django.urls import path
from .views import home,galeria,contacto,login,registrar, listado_usuarios,nuevo_usuarios,modificar_usuarios,eliminar_usuario

urlpatterns = [
    path('', home, name='home'),
    path('galeria/', galeria, name='galeria'),
    path('contacto/', contacto, name='contacto'),
    path('login/', login, name='login'),
    path('registrar/', registrar, name='registrar'),
    path('listado-usuarios/', listado_usuarios, name='listado_usuarios'),
    path('nuevo-usuarios/', nuevo_usuarios, name='nuevo_usuarios'),
    path('modificar-usuarios/<id>/', modificar_usuarios, name='modificar_usuarios'),
    path('eliminar-usuarios/<id>/', eliminar_usuario, name='eliminar_usuario'),
]