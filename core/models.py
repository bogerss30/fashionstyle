from django.db import models


# Create your models here.

class Usuario(models.Model):
    Nombre = models.CharField(max_length=80, null=True)
    Sexo = models.CharField(max_length=6, null=True)
    Correo_Electronico = models.CharField(max_length=100, null=True)
    Contraseña = models.CharField(max_length=30, null=True)
    Rut = models.IntegerField( null=True)

    def __str__(self):
        return self.Nombre

    
